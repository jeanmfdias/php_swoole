# PHP Swoole

To Setup

The first, build the container
`` docker build -t php-swoole . ``

After run the container, enter on the bash
`` docker run -it -v $(pwd):/app -w /app -p 8080:8080 php-swoole bash ``
